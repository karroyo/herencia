package arroyo.keissy.bl;

public class PersonaJuridica extends Persona {

    private String reprsentanteLegal;
    private String industria;

    public PersonaJuridica() {
        //Se llama al constructor de la clase padre
        super();
    }

    public PersonaJuridica(String nombre, String identificacion, String direccion, String telefono,
            String reprsentanteLegal, String industria) {
        super(nombre, identificacion, direccion, telefono);
        this.reprsentanteLegal = reprsentanteLegal;
        this.industria = industria;
    }

    public String getReprsentanteLegal() {
        return reprsentanteLegal;
    }

    public void setReprsentanteLegal(String reprsentanteLegal) {
        this.reprsentanteLegal = reprsentanteLegal;
    }

    public String getIndustria() {
        return industria;
    }

    public void setIndustria(String industria) {
        this.industria = industria;
    }

    @Override
    public String toString() {
        return "PersonaJuridica{" + "reprsentanteLegal=" + reprsentanteLegal + ", industria=" + industria + '}';
    }

}
