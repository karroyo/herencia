package arroyo.keissy.bl;

public class Persona {

    protected String nombre;
    protected String identificacion;
    protected String direccion;
    protected String telefono;

    public Persona() {

    }

    public Persona(String nombre, String identificacion, String direccion, String telefono) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.telefono = telefono;
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Persona:" + "Nombre:" + nombre + ", Direccion=" + direccion + ", Identificacion=" + identificacion + ", telefono=" + telefono + '.';
    }

}
