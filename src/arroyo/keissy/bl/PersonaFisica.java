package arroyo.keissy.bl;

import arroyo.keissy.bl.Persona;
import java.time.LocalDate;

public class PersonaFisica extends Persona {

    private LocalDate fecha;
    private String apellido;
    private int edad;
    private char genero;

    public PersonaFisica() {
        //Se llama al constructor de la clase padre
        super();
    }

    public PersonaFisica(String nombre, String identificacion, String direccion, String telefono,
            LocalDate fecha, String apellido, int edad, char genero) {
        //Se llama al constructor de la clase padre para que reciba parametros 
        super(nombre, identificacion, direccion, telefono);
        this.fecha = fecha;
        this.apellido = apellido;
        this.edad = edad;
        this.genero = genero;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "PersonaFisica{" + super.toString() + "fecha=" + fecha + ", apellido=" + apellido + ", edad=" + edad + ", genero=" + genero + '}';
    }

}
