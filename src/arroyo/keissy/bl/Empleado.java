package arroyo.keissy.bl;

import java.time.LocalDate;

public class Empleado {

    private String puesto;
    private LocalDate fechaContrataccion;
    private String nombre;
    private String direccion;
    private char genero;
    private String telefono;
    private String correo;

    public Empleado() {
        nombre = " ";
        puesto = " ";
        direccion = " ";
        telefono = " ";
        correo = " ";
    }

    public Empleado(String puesto, LocalDate fechaContrataccion, String nombre,
            String direccion, char genero, String telefono, String correo) {

        this.nombre = nombre;
        this.puesto = puesto;
        this.fechaContrataccion = fechaContrataccion;
        this.telefono = telefono;
        this.direccion = direccion;

    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public LocalDate getFechaContrataccion() {
        return fechaContrataccion;
    }

    public void setFechaContrataccion(LocalDate fechaContrataccion) {
        this.fechaContrataccion = fechaContrataccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "puesto=" + puesto + ", fechaContrataccion=" + fechaContrataccion + ", nombre=" + nombre + ", direccion=" + direccion + ", genero=" + genero + ", telefono=" + telefono + ", correo=" + correo + '}';
    }

}
