
package arroyo.keissy.iu;

import arroyo.keissy.bl.Persona;
import arroyo.keissy.bl.PersonaFisica;
import java.io.*;
import java.time.LocalDate;
import arroyo.keissy.bl.PersonaJuridica;

public class Main {

    static BufferedReader leer = new BufferedReader(
            new InputStreamReader(System.in));
    static PrintStream imprimir = System.out;
    //static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {

        imprimir.println("Digite el nombre");
        String nombre = leer.readLine();

        imprimir.println("Digite la identificación");
        String identificacion = leer.readLine();

        imprimir.println("Digite la dirección");
        String direccion = leer.readLine();

        imprimir.println("Digite el telefono");
        String telefono = leer.readLine();

        Persona p = new Persona(nombre, identificacion, direccion, telefono);
        imprimir.println(p.toString());

        //PersonaFisica
        imprimir.println("Digite la fecha");
        imprimir.println("Con el formato YYYY-MM-DD");
        String fechaNInput = leer.readLine();
        LocalDate fechaNac = LocalDate.parse(fechaNInput);

        imprimir.println("Digite el apellido");
        String apellidos = leer.readLine();

        imprimir.println("Digite la edad");
        int edad = Integer.parseInt(leer.readLine());

        imprimir.println("Digite el genero");
        char genero = leer.readLine().charAt(0);

        PersonaFisica pf = new PersonaFisica(nombre, identificacion, direccion, telefono,
                fechaNac, apellidos, edad, genero);
        imprimir.println(pf.toString());

        //PersonaJuridica
        imprimir.println("Digite el representante legal");
        String representanteLegal = leer.readLine();

        imprimir.println("Digite la industria");
        String industria = leer.readLine();

        PersonaJuridica pj = new PersonaJuridica(nombre, identificacion, direccion, telefono,
                representanteLegal, industria);
        imprimir.println(pj.toString());

        p = pf;
        pf = (PersonaFisica) p;
    }//Fin de main

}//Fin del programa
